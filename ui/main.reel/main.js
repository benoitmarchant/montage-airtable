/**
 * @module ui/main.reel
 */
var Component = require("montage/ui/component").Component,
    DataService = require("montage/data/service/data-service").DataService,
    applicationService = require("data/main.datareel/main.mjson").montageObject,
    Furniture = require("data/main.datareel/model/furniture").Furniture;

/**
 * @class Main
 * @extends Component
 */
exports.Main = Component.specialize(/** @lends Main# */ {
    constructor: {
        value: function Main() {
            this.super();
            DataService.authorizationManager.delegate = this;
            this._initializeServices();
            this._fetchObjects();
        }
    },

    _initializeServices: {
        value: function () {
            this.application.service = applicationService;
            this.isReady = true;
        }
    },
    _fetchObjects: {
        value: function () {

            var self = this;
            return this.application.service.fetchData(Furniture).then(function (furniture) {
                self.furniture = furniture;
                return null;
            });

        }
    }
});
